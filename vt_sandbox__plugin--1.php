<?php
/*

 * Plugin Name: VT Plugin 1 Dashboard Empty
 * Description: This is an empty plugin that does nothing. Completely useless. Oh just a dashboard message.
 * Version: 1.3.0
 * Author: Vania Tejuco
 * Author URI: https://www.vaniatejuco.com
 * Text Domain: vt_sandbox__plugin--1
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt

 */

namespace vt_sandbox\msg;

// stop unwanted visitors calling directly

if (!defined('ABSPATH')) {
	exit('Go Away!!');
}

class mydashboard {
	public function __construct() {
		/*
			* Action the widget
			*
			*/
		add_action('wp_dashboard_setup', array($this, 'my_dashboard_widgets'));
	}
	/*
		 * Create the message
		 *
		 */

	function dashboard_first_msg() {
		echo '<p>Welcome to your plugin <br/> -happy days </p>';
	}

	/*
		 * setup the widget
		 *
		 */

	function my_dashboard_widgets() {
		wp_add_dashboard_widget('custom_help_widget', 'My Dashboard Message', array($this, 'dashboard_first_msg'));
	}
}

$start = new mydashboard();
